# Davon's Atlassian Coding Exercise

This application was built using custom components that I've made over the years. The frontend is served by an Express server, which also holds the API that serves the ```spaces```. 

**You'll find that the API supports calls to every specification on the .raml, however it returns a serialized version, rather than the full object.**

Frontend built with:

*   **React** - Single-page application
*   **MobX** - State Management, similar to Redux. You'll see me keeping track of global states in the AppStore.ts by using an ```@observable``` decoration, and then applying an ```@observer``` decoration to components that should track the global states.
*   **React Router 4** - URL routing for single-page applications.
*   **Typescript** - Static typing for Javascript.

Backend built with:

*   **Express** - JS server that holds the API.

## Running the application

1.  Make sure that Node.js is installed. This application was built on v7.9.0, but should work on later versions of Node.
2.  Run ```npm install``` to install of the necessary packages.
3.  Run ```npm run start:demo``` to start the application.
4.  Navigate to http://localhost:8080 on any browser.

## Running tests

1.  Make sure that the application is running (you need to be able to make calls to http://localhost:8080).
2.  Run ```npm run test``` to start the Jest testing suite.

## Mocking erroneous API calls

To do this, we'll need to run the development environment *without* the backend server, so that it returns a 504 when we try to make calls to ```/api/spaces```.

1.  Run ```npm run start:dev``` to run the frontend as a standalone.
2.  Make sure that you don't have any instances of ```npm run start:backend``` or ```npm run start:demo``` running in your terminal, as that will connect to the frontend.
3.  You should see a screen that tells you that you don't have spaces. 




