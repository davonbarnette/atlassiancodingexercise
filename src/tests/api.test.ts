import APIManager from "../frontend/managers/APIManager";



describe('Async Call: Get spaces and their entries/assets', () => {

   test('Expect spaces to be defined and that it has entries and assets', async () => {

       let spaces = await APIManager.getSpaces();
       expect(spaces).toBeDefined();

       let entries = await APIManager.getEntriesForSpace(spaces[0].id);
       expect(entries).toBeDefined();

       let assets = await APIManager.getAssetsForSpace(spaces[0].id);
       expect(assets).toBeDefined();

   })

});

