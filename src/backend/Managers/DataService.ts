import {AssetData, EntryData, SpaceData, UserData} from "../../globals/types";
import Serializers from "./Serializers";
import Database from "./FakeDatabaseORM";

export default class DataService {

    static async getAllSpaces(req, res){
        let raw = Database.Spaces.findAll();

        let spaces:SpaceData[] = raw.map(rawSpace => {
            return Serializers.Space(rawSpace);
        });

        res.status(200).send(spaces);
    }
    static async getSpaceById(req,res){
        let spaceId = req.params.spaceId;
        let space = Serializers.Space(Database.Spaces.findOne(spaceId));
        res.status(200).send(space);

    }

    static async getEntriesInSpace(req,res){
        let spaceId = req.params.spaceId;
        let raw = Database.Entries.findAllEntriesInSpace(spaceId);

        let entries:EntryData[] = raw.map( rawEntry => {
            return Serializers.Entry(rawEntry);
        });

        res.status(200).send(entries);

    }
    static async getEntryById(req,res){
        let spaceId = req.params.spaceId;
        let entryId = req.params.entryId;

        let entry = Serializers.Entry(Database.Entries.findOneEntryInSpace(spaceId, entryId));
        res.status(200).send(entry);
    }

    static async getAssetsInSpace(req,res){
        let spaceId = req.params.spaceId;

        let raw = Database.Assets.findAllAssetsInSpace(spaceId);
        let assets:AssetData = raw.map( rawAsset => {
            return Serializers.Asset(rawAsset);
        });

        res.status(200).send(assets);

    }
    static async getAssetById(req,res){
        let spaceId = req.params.spaceId;
        let assetId = req.params.assetId;

        let asset = Serializers.Asset(Database.Assets.findOneAssetInSpace(spaceId, assetId));
        res.status(200).send(asset);

    }

    static async getUsers(req,res){
        let raw = Database.Users.findAll();
        let users:UserData[] = raw.map( rawUser => {
            return Serializers.User(rawUser);
        });
        res.status(200).send(users);
    }
    static async getUserById(req,res){
        let userId = req.params.userId;

        let user = Serializers.User(Database.Users.findOne(userId));
        res.status(200).send(user);

    }


}


