import {AssetData, EntryData, SpaceData, UserData} from "../../globals/types";

export default class Serializers {

    static Space(raw):SpaceData{
        return {
            title:raw.fields.title,
            description:raw.fields.description,
            id:raw.sys.id,
            createdAt:raw.sys.createdAt,
            createdBy:raw.sys.createdBy,
            updatedAt:raw.sys.updatedAt,
            updatedBy:raw.sys.updatedBy,
        }
    }
    static Entry(raw):EntryData{
        return {
            title:raw.fields.title,
            summary:raw.fields.summary,
            body:raw.fields.body,
            id:raw.sys.id,
            createdAt:raw.sys.createdAt,
            createdBy:raw.sys.createdBy,
            updatedAt:raw.sys.updatedAt,
            updatedBy:raw.sys.updatedBy,
            version:raw.sys.version,
            space:raw.sys.space,
        }
    }
    static Asset(raw):AssetData{
        return {
            title:raw.fields.title,
            contentType:raw.fields.contentType,
            fileName:raw.fields.fileName,
            upload:raw.fields.upload,
            id:raw.sys.id,
            createdAt:raw.sys.createdAt,
            createdBy:raw.sys.createdBy,
            updatedAt:raw.sys.updatedAt,
            updatedBy:raw.sys.updatedBy,
            version:raw.sys.version,
            space:raw.sys.space,
        }
    }
    static User(raw):UserData {
        return {
            name:raw.fields.name,
            role:raw.fields.role,
            id:raw.sys.id,
        }
    }

}