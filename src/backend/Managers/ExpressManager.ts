import * as express from 'express';
import * as path from 'path';
import {api} from "../routes";
import {SETTINGS} from "../BackendSettings";
import Logging from "../Logging";

export class ExpressManagerClass {

    app;

    constructor(){

        Logging.info('[Express] Initializing...');

        this.app = express();
        this.applyMiddleware(this.app);

        Logging.success('[Express] Initialized');
    }

    applyMiddleware(app){
        app.use((req, res, next)=>{
            res.setHeader(`Access-Control-Allow-Origin`, `*`);
            res.setHeader(`Access-Control-Allow-Credentials`, `true`);
            res.setHeader(`Access-Control-Allow-Methods`, `GET,HEAD,OPTIONS,POST,PUT,DELETE`);
            res.setHeader(`Access-Control-Allow-Headers`, `Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers`);
            res.setHeader(`Cache-Control`, `no-cache`);
            next();
        });

        app.use(express.json({limit:SETTINGS.EXPRESS.JSON_LIMIT}));
        app.use(express.urlencoded({limit:SETTINGS.EXPRESS.URL_ENCODED_LIMIT, extended:true}));
        app.use(express.static(SETTINGS.EXPRESS.STATIC_BUILD_PATH));
        app.use(SETTINGS.EXPRESS.API_ROOT_PATH, api);
        app.get('*', (req, res) => res.sendFile(path.resolve(SETTINGS.EXPRESS.STATIC_BUILD_PATH + '/' + SETTINGS.EXPRESS.INDEX_FILE)) );

    }

}