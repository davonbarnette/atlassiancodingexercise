import * as moment from 'moment';
import * as chalk from 'chalk';

export default class Logging {

    static multipleConsoleArgs(message, style, ...args){

        message = "%c" + message.toUpperCase();

        args.unshift(style);
        args.unshift(message);
        console.log.apply(this, args);

    }
    static getMoment(){
        return `[${moment().format('YYYY-MM-DD HH:mm:ssZZ')}]`
    }

    static info(message, ...args){

        if (args.length === 0) console.log(chalk.blue(Logging.getMoment() + "[        INFO]  " + message));
        else console.log(chalk.blue(Logging.getMoment() + "[        INFO]  " + message), ...args);

    }

    static success(message, ...args){

        if (args.length === 0) console.log(chalk.green(Logging.getMoment() + "[     SUCCESS]  " + message));
        else console.log(chalk.green(Logging.getMoment() + "[     SUCCESS]  " + message), ...args);

    }

    static error(message, ...args){

        if (args.length === 0) console.log(chalk.red(Logging.getMoment() + "[       ERROR]  " + message));
        else console.log(chalk.red(Logging.getMoment() + "[       ERROR]  " + message), ...args);

    }

}