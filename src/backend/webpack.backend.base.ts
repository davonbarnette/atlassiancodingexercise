let webpack = require('webpack');
let path = require('path');
let fs = require('fs');
let rootPath = path.join(__dirname,"../../builds");
let nodeModules = {};

fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    entry: './src/backend/index.ts',

    target: 'node',
    output: {
        path: path.join(rootPath, 'backend'),
        filename: 'backend.js'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    externals: nodeModules,
    plugins: [
        new webpack.IgnorePlugin(/\.(css|less)$/),
        new webpack.HotModuleReplacementPlugin(),
        // new webpack.BannerPlugin({ banner:'require("source-map-support").install();', raw: true, entryOnly: false })
    ],
    resolve: {
        extensions: ['.json', '.js', '.ts']
    },
    // devtool: 'sourcemap'
};