import * as http from 'http';
import { ExpressManagerClass } from "./Managers/ExpressManager";
import { SETTINGS } from "./BackendSettings";
import Logging from "./Logging";

class ApplicationSingleton {

    express:ExpressManagerClass;
    server;

    constructor(){
        this.init();
    }


    async init(){
        Logging.info('[Application] Initializing...');
        this.express = new ExpressManagerClass();
        this.server = http.createServer(this.express.app);
        this.server.listen(SETTINGS.EXPRESS.PORT, () => Logging.info('[Server] Listening on port > ', SETTINGS.EXPRESS.PORT));

    }
}

const Application = new ApplicationSingleton();
export default Application;








