export const SETTINGS = {

    EXPRESS: {
        JSON_LIMIT:'10mb',
        URL_ENCODED_LIMIT:'10mb',

        PORT:8080,

        STATIC_BUILD_PATH: 'builds/frontend',
        INDEX_FILE:'index.html',
        API_ROOT_PATH:'/api'
    }
};