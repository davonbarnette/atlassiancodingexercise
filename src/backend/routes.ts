import * as express from 'express';
import DataService from "./Managers/DataService";

export const api = express.Router();

api.route('/space').get(DataService.getAllSpaces);

api.route('/space/:spaceId').get(DataService.getSpaceById);

api.route('/space/:spaceId/entries').get(DataService.getEntriesInSpace);

api.route('/space/:spaceId/entries/:entryId').get(DataService.getEntryById);

api.route('/space/:spaceId/assets').get(DataService.getAssetsInSpace);

api.route('/space/:spaceId/assets/:assetId').get(DataService.getAssetById);

api.route('/users').get(DataService.getUsers);

api.route('/users/:userId').get(DataService.getUserById);