import {observable} from 'mobx';
import { createBrowserHistory } from 'history'
import {SpacesManager} from "./managers/SpacesManager";
import AppActions from "./AppActions";
import {EntriesManager} from "./managers/EntriesManager";
import {AssetsManager} from "./managers/AssetsManager";
import UsersManager from "./managers/UsersManager";

class AppStoreSingleton{

    @observable spaces:SpacesManager;
    @observable entries:EntriesManager;
    @observable assets:AssetsManager;
    @observable users:UsersManager;

    history:any;

    constructor(){
        this.history = createBrowserHistory()
    }
    init(){
        AppActions.getInitialData();
    }
}

let AppStore = new AppStoreSingleton();
export default AppStore;