import { observable } from 'mobx';
import {SpaceData} from "../../globals/types";

export class SpacesManager {

    @observable spacesById:Map<string, SpaceData> = new Map();

    constructor(spaces:SpaceData[]){
        this.setMultiple(spaces);
    }
    getOne(spaceId:string){
        return this.spacesById.get(spaceId);
    }

    setOne(space:SpaceData){
        this.spacesById.set(space.id, space);
    }
    setMultiple(spaces:SpaceData[]){
        if (spaces.length === 0) return;
        for (let i = 0; i < spaces.length; i++) {
            let space = spaces[i];
            this.setOne(space);
        }
    }
    get all():SpaceData[]{
        return [...this.spacesById.values() as any]
    }
}