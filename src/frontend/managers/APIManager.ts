import axios from 'axios';
import {AssetData, EntryData, SpaceData} from "../../globals/types";

export default class APIManager {

    static async getSpaces():Promise<SpaceData[]>{
        try {
            let res = await axios.get(APIRoutes.baseSpaceRoute);
            if (res.status === 200) return res.data;
        }
        catch(e){ }
    }

    static async getEntriesForSpace(spaceId:string):Promise<EntryData[]>{
        try {
            let res = await axios.get(APIRoutes.getEntriesBySpaceId(spaceId));
            if (res.status === 200) return res.data;
        }
        catch(e){ }
    }
    static async getAssetsForSpace(spaceId:string):Promise<AssetData[]>{
        try {
            let res = await axios.get(APIRoutes.getAssetsBySpaceId(spaceId));
            if (res.status === 200) return res.data;
        }
        catch(e){ }
    }
    static async getUsers(){
        try {
            let res = await axios.get(APIRoutes.baseUserRoute);
            if (res.status === 200) return res.data;
        }
        catch(e){ }
    }

}

class APIRoutes {

    static base = '/api';
    static space = '/space';
    static entries = '/entries';
    static assets = '/assets';
    static users = '/users';

    static get baseSpaceRoute(){
        return `${APIRoutes.base}${APIRoutes.space}`;
    }
    static get baseUserRoute(){
        return `${APIRoutes.base}${APIRoutes.users}`;
    }
    static getEntriesBySpaceId(spaceId:string){
        return `${APIRoutes.baseSpaceRoute}/${spaceId}${APIRoutes.entries}`
    }
    static getAssetsBySpaceId(spaceId:string){
        return `${APIRoutes.baseSpaceRoute}/${spaceId}${APIRoutes.assets}`
    }

}