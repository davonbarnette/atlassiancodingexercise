import { observable } from 'mobx';
import {AssetData} from "../../globals/types";

export class AssetsManager {

    @observable assetsBySpaceId:Map<string, AssetData[]> = new Map();

    getOne(spaceId:string){
        return this.assetsBySpaceId.get(spaceId);
    }

    setOne(spaceId:string, assets:AssetData[]){
        this.assetsBySpaceId.set(spaceId, assets);
    }

    hasOne(spaceId:string){
        this.assetsBySpaceId.has(spaceId);
    }



    get all():AssetData[]{
        return [...this.assetsBySpaceId.values() as any]
    }
}