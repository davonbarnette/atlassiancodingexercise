import { observable } from 'mobx';
import {AssetData, EntryData} from "../../globals/types";

export class EntriesManager {

    @observable entriesBySpaceId:Map<string, EntryData[]> = new Map();

    getOne(spaceId:string){
        return this.entriesBySpaceId.get(spaceId);
    }

    setOne(spaceId:string, entries:EntryData[]){
        this.entriesBySpaceId.set(spaceId, entries);
    }

    hasOne(spaceId:string){
        this.entriesBySpaceId.has(spaceId);
    }


    get all():EntryData[]{
        return [...this.entriesBySpaceId.values() as any]
    }
}