export class RouterPaths {


    static SPACE = '/space';
    static SPACE_ID_ROUTE = ':spaceId';

    static get spaceIdParamRoute(){
        return `${RouterPaths.SPACE}/${RouterPaths.SPACE_ID_ROUTE}`;
    }

    static getSpaceIdRoute(spaceId:string){
        return `${RouterPaths.SPACE}/${spaceId}`;
    }


}