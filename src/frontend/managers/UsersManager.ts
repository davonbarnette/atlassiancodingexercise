import { observable } from 'mobx';
import {UserData} from "../../globals/types";

export default class UsersManager {

    @observable usersById:Map<string, UserData> = new Map();

    constructor(users:UserData[]){
        this.setMultiple(users);
    }
    getOne(userId:string){
        return this.usersById.get(userId.toLowerCase());
    }

    setOne(user:UserData){
        this.usersById.set(user.id.toLowerCase(), user);
    }
    setMultiple(users:UserData[]){
        if (users.length === 0) return;
        for (let i = 0; i < users.length; i++) {
            let user = users[i];
            this.setOne(user);
        }
    }
    get all():UserData[]{
        return [...this.usersById.values() as any]
    }

}