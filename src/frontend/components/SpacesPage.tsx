import * as React from 'react';
import {observer} from 'mobx-react'
import AppStore from "../AppStore";
import {RouteComponentProps, withRouter} from "react-router";
import TableComponentFlex from "./TableComponentFlex";
import * as cx from 'classnames';
import * as Icon from 'react-feather';
import AppActions from "../AppActions";
import {AssetsManager} from "../managers/AssetsManager";
import {AssetData, EntryData} from "../../globals/types";

interface NavButtonProps{
    onClick:(e)=>void,
    display:string,
    selected:boolean,
    svg?:any,
}

const Tab:React.SFC<NavButtonProps> = (props:NavButtonProps) => {
    let color = '#d0d0d0';
    if (props.selected) color = '#757575';
    return (
        <div className={cx('tab', {selected: props.selected})} onClick={props.onClick}>
            <div className='svg-container'>{props.svg(color)}</div>
            <div className='text'>{props.display}</div>
        </div>
    )
};

const TABS = { ENTRIES: 'ENTRIES', ASSETS: 'ASSETS' };
const TAB_RENDERABLES = [
    {display:'Entries', id:TABS.ENTRIES, svg:(color) => <Icon.List color={color} size={18}/>},
    {display:'Assets', id:TABS.ASSETS, svg:(color) => <Icon.Layers color={color} size={18}/>}
];

interface SpacesPageProps extends RouteComponentProps {  }
interface SpacesPageState { selectedTab:string }

@observer
class SpacesPage extends React.Component<SpacesPageProps, SpacesPageState>{

    state:SpacesPageState = { selectedTab: TABS.ENTRIES };

    get curSpaceIdFromUrl(){
        let params:any = this.props.match.params;
        return params.spaceId;
    }
    get curSpace(){
        return AppStore.spaces.getOne(this.curSpaceIdFromUrl);
    }

    get table():{ headers: string[], rows:any[] }{
        if (this.state.selectedTab === TABS.ENTRIES) return this.getEntriesDataForTable(this.curSpaceIdFromUrl);
        else if (this.state.selectedTab === TABS.ASSETS) return this.getAssetsDataForTable(this.curSpaceIdFromUrl);
    }

    handleOnTabClick(e, tab:string){
        this.setState({selectedTab:tab});
    }

    get tabs(){
        return TAB_RENDERABLES.map( tab => (
            <Tab onClick={(e)=>this.handleOnTabClick(e, tab.id)}
                 display={tab.display}
                 key={tab.id}
                 svg={tab.svg}
                 selected={this.state.selectedTab === tab.id}/>
        ))
    }

    getAssetsDataForTable(spaceId:string){
        let assets = AppStore.assets.getOne(spaceId);

        if (!assets) return { headers:null, rows:null };

        let headers = ['Title', 'Content Type', 'File Name', 'Created By', 'Updated By', 'Last Updated'];

        let rows = assets.map((asset:AssetData) => {
            let createdBy = AppStore.users.getOne(asset.createdBy).name;
            let updatedBy = AppStore.users.getOne(asset.updatedBy).name;
            return [asset.title, asset.contentType, asset.fileName, createdBy, updatedBy, asset.updatedAt];
        });

        return { headers, rows };
    }

    getEntriesDataForTable(spaceId:string){
        let entries = AppStore.entries.getOne(spaceId);
        if (!entries) return { headers:null, rows:null };

        let headers = ['Title', 'Summary', 'Created By', 'Updated By', 'Last Updated'];

        let rows = entries.map((entry:EntryData) => {
            let createdBy = AppStore.users.getOne(entry.createdBy).name;
            let updatedBy = AppStore.users.getOne(entry.updatedBy).name;

            return [entry.title, entry.summary, createdBy, updatedBy, entry.updatedAt];
        });

        return { headers, rows };
    }

	render() {
        if (!AppStore.spaces || !AppStore.entries || !AppStore.assets || !AppStore.users) return null;
        if (!this.curSpace) return null;

		return (
			<div className='spaces-page-container'>
                <div className='header'>{this.curSpace.title}</div>
                <div className='navigation-component'>
                    <div className='tabs'>{this.tabs}</div>
                </div>
                <TableComponentFlex headers={this.table.headers} rows={this.table.rows}/>
            </div>
		)
	}
}

export default withRouter<RouteComponentProps>(SpacesPage);
