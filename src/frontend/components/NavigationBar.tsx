import * as React from 'react';
import { observer } from 'mobx-react';
import * as cx from 'classnames';
import {RouteComponentProps, withRouter} from "react-router";
import AppActions from "../AppActions";
import AppStore from "../AppStore";
import {SpaceData} from "../../globals/types";
import {split} from "ts-node";

interface NavButtonProps{
    onClick:(e)=>void,
    buttonText:string,
    selected:boolean,
}

const NavButton:React.SFC<NavButtonProps> = (props) => {
    return (
        <div className={cx('navigate-to', {selected: props.selected})} onClick={props.onClick}>
            <div className='selector'/>
            <div className='text'>{props.buttonText}</div>
        </div>
    )
};

interface NavigationComponentProps extends RouteComponentProps { }

@observer
class NavigationComponent extends React.Component<NavigationComponentProps> {

    props: NavigationComponentProps;

    constructor(props: NavigationComponentProps) {
        super(props);
    }

    handleOnNavigateClick(e,spaceId){
        e.stopPropagation();
        AppActions.selectSpace(spaceId);
    }

    isCurrentPath(path:string){
        let splitBySlash = this.props.location.pathname.split('/');
        let last = splitBySlash[splitBySlash.length - 1];
        return last === path;
    }

    renderNavButtons(){
        let spaces = AppStore.spaces.all;

        return spaces.map((space: SpaceData) => (
            <NavButton
                onClick={(e) => this.handleOnNavigateClick(e, space.id)}
                buttonText={space.title}
                key={space.id}
                selected={this.isCurrentPath(space.id)}/>
        ))
    }
    render() {

        if (!AppStore.spaces) return null;

        return (
            <div className='navigation-bar'>
                <div className='nav-buttons'>
                    {this.renderNavButtons()}
                </div>
            </div>
        )
    }
}

export default withRouter<NavigationComponentProps>(NavigationComponent);