import * as React from 'react';
import * as cx from 'classnames';
import {SortingData} from "../../globals/types";
import LogManager from "../../globals/LogManager";

interface TableComponentFlexProps {
    headers:string[],
    rows:any[][],
    accentuateFirstColumn?:boolean,
    onRowClick?:(rowId)=>any,
    onSearchChange?:(...args)=>any,
    filters?:any[]
}

interface TableComponentFlexState {
    sortBy:SortingData,
}

export default class TableComponentFlex extends React.Component<TableComponentFlexProps, TableComponentFlexState> {

    state:TableComponentFlexState = {
        //Initial table state is sorted by first column, descending
        sortBy: { columnIndex:0, descending:true },
    };

    handleOnHeaderClick(headerIndex){
        let descending = true;

        LogManager.componentLog('sortBy', this.state.sortBy);

        // If the user clicks an already sorted column, sort in the opposite direction
        if (this.state.sortBy.columnIndex === headerIndex) descending = !this.state.sortBy.descending;

        // We can assume the data column that needs to be sorted is the same index as the header
        let sortBy:SortingData = {columnIndex:headerIndex, descending};

        this.setState({ sortBy })
    }

    getFlexedColumnWidthPercentage(firstColumn = false):number{
        //Use this method to get the flex: 1 1 {}% property
        //Returns a whole number (0-100)

        let numColumns = this.props.headers.length;
        let equallySeparatedColumnWidth = 100 / numColumns;

        if (this.props.accentuateFirstColumn){
            let newFirstColumnWidthPercentage = equallySeparatedColumnWidth * 2;

            //Accentuated first columns get twice the width they'd normally get
            if (firstColumn) return newFirstColumnWidthPercentage;

            //If we're on a column other than the first, we take the new firstColumn width, subtract it from the
            //total and then divide the rest of the space between the remaining columns
            else if (numColumns === 2) return 30;
            else return (100 - newFirstColumnWidthPercentage) / (numColumns - 1);
        }

        return 100 / numColumns;
    }

    renderHeaderRowItems(){
        return this.props.headers.map((header, index) => (
            <div className={cx('header-item', {main:index === 0 && this.props.accentuateFirstColumn})}
                 style={{flex: `1 1 ${this.getFlexedColumnWidthPercentage(index === 0)}%`}}
                 key={index}
                 onClick={()=>this.handleOnHeaderClick(index)}>{header}</div>
        ))
    }

    renderDataRows(){
        let rows = this.props.rows;
        let descending = this.state.sortBy.descending;

        // Sort the based on the current sorting state
        let sorted = rows.sort((a, b)=> {

            let columnIndex = this.state.sortBy.columnIndex;

            let columnCellA = a[columnIndex].toLowerCase();
            let columnCellB = b[columnIndex].toLowerCase();

            if (descending){
                if (columnCellB > columnCellA) return -1;
                else return 1;
            }

            else if (!descending){
                if (columnCellB < columnCellA) return -1;
                else return 1;

            }

        });

        return sorted.map((dataRow, index) => {
            return (
                <div className='data-row' key={index}>
                    {this.renderDataRowItems(dataRow)}
                </div>
            )
        })
    }

    renderDataRowItems(data:any[]){
        return data.map((dataItem, index) => (
            <div className='data-item'
                 key={index}
                 style={{flex: `1 1 ${this.getFlexedColumnWidthPercentage(index === 0)}%`}}>{dataItem}</div>
        ))
    }
    render() {

        if (!this.props.headers || !this.props.rows) return (
            <div className='no-entries-container'>
                <div className='text'>No Entries</div>
            </div>
        );

        return (
            <div className='table-component-flex'>
                <div className='header-row'>{this.renderHeaderRowItems()}</div>
                <div className='data-rows-outer'>
                    <div className='data-rows-container'>{this.renderDataRows()}</div>
                </div>

            </div>
        )
    }

}