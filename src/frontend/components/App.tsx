import * as React from 'react';
import {observer} from 'mobx-react'
import './app.css'
import { Route, Router, Switch, Redirect} from 'react-router';
import AppStore from "../AppStore";
import NavigationBar from "./NavigationBar";
import {RouterPaths} from "../managers/RouterPaths";
import SpacesPage from "./SpacesPage";

@observer
export default class AppComponent extends React.Component{

    get firstSpace(){
        return AppStore.spaces.all[0];
    }

	render() {

        if (AppStore.spaces === null) return (
            <div className='app-component'>
                <div className='no-spaces'>
                    Sorry, looks like we couldn't access your spaces at this time.
                </div>
            </div>
        );
        if (AppStore.spaces === undefined) return (
            <div className='app-component'>
                <div className='no-spaces'>
                    Loading...
                </div>
            </div>
        );

		return (
			<Router history={AppStore.history}>
				<Route render={({ location }) => (
					<div className="app-component">
						<div className='app-body'>
							<NavigationBar/>
                            <Switch>
                                <Route exact path={RouterPaths.spaceIdParamRoute} component={SpacesPage}/>
                                <Redirect to={RouterPaths.getSpaceIdRoute(this.firstSpace.id)}/>
                            </Switch>
						</div>
					</div>
                )}/>
			</Router>
		)
	}
}
