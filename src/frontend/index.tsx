import * as React from 'react';
import * as ReactDOM from 'react-dom';

import AppStore from "./AppStore";
AppStore.init();

import AppComponent from "./components/App";

ReactDOM.render(
    <AppComponent />,
	document.getElementById('root') as HTMLElement
);

