import AppStore from "./AppStore";
import {action} from 'mobx';
import {SpacesManager} from "./managers/SpacesManager";
import APIManager from "./managers/APIManager";
import {EntriesManager} from "./managers/EntriesManager";
import {AssetsManager} from "./managers/AssetsManager";
import {RouterPaths} from "./managers/RouterPaths";
import UsersManager from "./managers/UsersManager";

export default class AppActions {

    @action static async getInitialData(){

        let spaces = await APIManager.getSpaces();
        if (spaces) {
            AppStore.spaces = new SpacesManager(spaces);
            AppActions.selectSpace(spaces[0].id);
        }
        else AppStore.spaces = null;

        let users = await APIManager.getUsers();
        if (users) AppStore.users = new UsersManager(users);
        else AppStore.users = null;

    }

    @action static async selectSpace(spaceId:string){

        AppActions.redirectTo(RouterPaths.getSpaceIdRoute(spaceId));

        AppActions.getEntriesForSpace(spaceId);
        AppActions.getAssetsForSpace(spaceId);
    }

    @action static async getEntriesForSpace(spaceId:string){
        let entries = await APIManager.getEntriesForSpace(spaceId);
        if (entries){
            if (!AppStore.entries) AppStore.entries = new EntriesManager();
            AppStore.entries.setOne(spaceId, entries);
        }
    }

    @action static async getAssetsForSpace(spaceId:string){
        let assets = await APIManager.getAssetsForSpace(spaceId);
        if (assets){
            if (!AppStore.assets) AppStore.assets = new AssetsManager();
            AppStore.assets.setOne(spaceId, assets);
        }
    }

    @action static async getUsers(){
        let users = await APIManager.getUsers();
        if (users){

        }
    }

    @action static redirectTo(url:string){
        window.scrollTo(0,0);
        AppStore.history.push(url, {changed:true});
    }
}