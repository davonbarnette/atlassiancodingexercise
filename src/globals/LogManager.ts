import * as moment from 'moment';


class LogManagerSingleton {


    showLogs:string = localStorage.getItem('logs');
    logs:any[];

    constructor(){
        this.logs = [];
    }

    multipleConsoleArgs(message, style, ...args){

        this.logs.push({message, ...args});

        if (this.showLogs != "true") return;

        message = "%c" + message.toUpperCase();

        args.unshift(style);
        args.unshift(message);
        console.log.apply(this, args);

    }
    get Moment(){
        return `[${moment().format('YYYY-MM-DD HH:mm:ssZZ')}]`
    }

    actionLog(message, ...args){
        let style = "color:red";
        message = this.Moment + "[       ACTION]  " + message;
        this.multipleConsoleArgs(message, style, ...args);
    }

    requestLog(message, ...args){
        let style = "color:blue";
        message = this.Moment + "[        REQUEST]  " + message;
        this.multipleConsoleArgs(message, style, ...args);
    }

    passiveLog(message, ...args){
        let style = "color:green";
        message = "--- " + message + " ---";
        this.multipleConsoleArgs(message, style, ...args);
    }

    componentLog(message, ...args){
        let style = "color:orange";
        message = this.Moment + "[      COMPONENT]  " + message;
        this.multipleConsoleArgs(message, style, ...args);
    }

    subsequentLog(message, ...args){
        this.multipleConsoleArgs(message, null, ...args);
    }
}

const LogManager = new LogManagerSingleton();
export default LogManager;

(window as any)['LogManager'] = LogManager;