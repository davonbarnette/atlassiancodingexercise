export interface SortingData {
    columnIndex: number, descending:boolean
}

export interface TableComponentRowData {

    id:string|number,
    data:any[],

}

export interface TableHeaderData {
    text:string,
    sortFunction:()=>void,
}

export interface SpaceData {
    title:string,
    id:string,
    description:string,
    createdAt:string,
    createdBy:string,
    updatedAt:string,
    updatedBy:string,

}

export interface EntryData {

    id:string,
    body:string,
    title:string,
    summary:string,
    createdAt:string,
    createdBy:string,
    updatedBy:string,
    updatedAt:string,
    version:number,
    space:string,

}

export interface AssetData {

    title:string,
    contentType:string,
    fileName:string,
    createdBy:string,
    updatedBy:string,
    updatedAt:string,
    upload:string,
    id:string,
    createdAt:string,
    version:number,
    space:string,

}

export interface UserData {
    name:string,
    role:string,
    id:string,
}
